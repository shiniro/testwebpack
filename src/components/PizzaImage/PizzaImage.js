import React from 'react';

import classes from './pizzaImage.css';
import PizzaImage from '../../assets/img/pepperoni-pizza_646c8548d527960e928ebe2f7f9da6c91547539924.jpg';

const pizzaImage = (props) => (
  <div className={ classes.pizzaImage } >
    <img src={PizzaImage} className={classes.image} />
  </div>
);

export default pizzaImage;