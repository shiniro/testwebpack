const path = require('path');
const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'), // absolute path of this file folder , dist is the folder where bundle should be render
		filename: 'bundle.js',
		publicPath: ''
  },
  devtool: 'cheap-module-eval-source-map',
  module: {
    rules: [
      {
        test: /\.js$/, // regular expression that tell which files will be affected
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        exclude: /node_modules/,
        use: [ // take an array of loaders
          { loader: 'style-loader' },
          { loader: 'css-loader',
            options: {
              importLoaders: 1,
              modules: {
                localIdentName: '[name]_[local]_[hash:base64:5]'
              }
            }
          },
          { loader: 'postcss-loader',
            options: {
              ident: 'postcss',
              plugins: () => [ // run plugin
                autoprefixer()
              ]
            }
          }
        ]
      },
      {
        test: /\.(png|jpe?g|gif)$/, // image
        loader: 'url-loader?limit=8000&name=images/[name].[ext]' // ? and specified options
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: __dirname + '/src/index.html',
      filename: 'index.html',
      inject: 'body'
    })
  ]
};